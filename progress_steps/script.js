const progress = document.getElementById('progress')
const prev = document.getElementById('prev')
const next = document.getElementById('next')
// .circle refers to circle class
const circles = document.querySelectorAll('.circle')


let currentActive = 1

// when next button is clicked
next.addEventListener('click', () => {
    // increment current active by 1
    currentActive++

    if(currentActive > circles.length) {
        currentActive = circles.length
    }

    console.log(currentActive)

    update()
})

// when prev button is clicked
prev.addEventListener('click', () => {
    // increment decrease active by 1
    currentActive--

    if(currentActive < 1) {
        currentActive = 1
    } 
    
    console.log(currentActive)

    update()
})

function update() {
    circles.forEach((circle,index) => {
        if(index < currentActive) {
            circle.classList.add('active')
        } else {
            circle.classList.remove('active')
        }
    })

    const actives = document.querySelectorAll('.active')
    // set the percentage for progress bar
    progress.style.width = ((actives.length -1)  / (circles.length -1 ) * 100) + '%'

    // change button disabled depending on current active
    if(currentActive === 1 ) {
        prev.disabled = true
    } else if(currentActive === circles.length) {
        next.disabled = true
    } else {
        prev.disabled = false
        next.disabled = false
    }
}