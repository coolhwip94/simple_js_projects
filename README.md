# Simple JS Projects
> Collection of simple JS projects to build from.


## Projects
---

- `_project_starter` : Template used to build projects from
- `expanding_cards` : Styling panels with html/css and javascript to expand on click event.
- `progress_steps` : Progress bar with increasing steps


## References
---

- https://github.com/bradtraversy/50projects50days