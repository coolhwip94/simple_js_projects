
// grab all panels
const panels = document.querySelectorAll('.panel')

panels.forEach( panel => {

    // loop through panels and add event listener
    panel.addEventListener('click', () => {
        // first remove active classes from everything
        removeActiveClasses()
        // then set the current panel as active
        panel.classList.add('active')
    })
})

// remove the active class from everything
function removeActiveClasses() {
    panels.forEach(panel => {
        panel.classList.remove('active')
    })
}